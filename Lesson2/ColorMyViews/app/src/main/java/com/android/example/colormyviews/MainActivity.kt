package com.android.example.colormyviews

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var dispPattern = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setListeners()
    }

    private fun setListeners() {

        val boxOneText = findViewById<TextView>(R.id.box_one_text)
        val boxTwoText = findViewById<TextView>(R.id.box_two_text)
        val boxThreeText = findViewById<TextView>(R.id.box_three_text)
        val boxFourText = findViewById<TextView>(R.id.box_four_text)
        val boxFiveText = findViewById<TextView>(R.id.box_five_text)
        val rootConstraintLayout = findViewById<View>(R.id.constraint_layout)

        val clickableViews: List<View> =
            listOf(
                boxOneText, boxTwoText, boxThreeText,
                boxFourText, boxFiveText, rootConstraintLayout
            )

        for (item in clickableViews) {
            item.setOnClickListener { makeColored() }
        }
    }

    private fun makeColored() {
        box_one_text.setBackgroundColor(Color.DKGRAY)
        box_two_text.setBackgroundColor(Color.GRAY)
        constraint_layout.setBackgroundColor(Color.LTGRAY)

        if (dispPattern == 0) {
            dispPattern = 1
            box_three_text.setBackgroundResource(R.color.my_red)
            box_four_text.setBackgroundResource(R.color.my_yellow)
            box_five_text.setBackgroundResource(R.color.my_green)
        }
        else if (dispPattern == 1) {
            dispPattern = 2
            box_three_text.setBackgroundResource(R.color.my_yellow)
            box_four_text.setBackgroundResource(R.color.my_green)
            box_five_text.setBackgroundResource(R.color.my_red)
        }
        else {
            dispPattern = 0
            box_three_text.setBackgroundResource(R.color.my_green)
            box_four_text.setBackgroundResource(R.color.my_red)
            box_five_text.setBackgroundResource(R.color.my_yellow)
        }
    }
}
