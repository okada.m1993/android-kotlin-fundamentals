# Android Kotlin Fundamentals

本プロジェクトは、Android Kotlin Fundamentals(https://developer.android.com/courses/kotlin-android-fundamentals/overview)
のオンラインレッスンで書いたコード

Lesson 1: Build your first app
  Android Kotlin Fundamentals: Get started
    8. Coding challenge
    →HelloWorld

  Android Kotlin Fundamentals 01.2: Anatomy of Basic Android Project
  Android Kotlin Fundamentals 01.3: Image resources and compatibility
    8. Coding challenge
    →DiceRoller

  Android Kotlin Fundamentals 01.4: Learn to help yourself
  →sunflower-main

Lesson 2: Layouts
  Android Kotlin Fundamentals: LinearLayout using the Layout Editor
    10. Coding challenge
  Android Kotlin Fundamentals: Add user interactivity
    →AboutMe

  Android Kotlin Fundamentals: 02.3 ConstraintLayout using the Layout Editor
    9. Coding challenge
    13. Coding challenge
    →ColorMyViews

  Android Kotlin Fundamentals 02.4: Data binding basics
  →AboutMeDataBinding-Starter

Lesson 3: Navigation